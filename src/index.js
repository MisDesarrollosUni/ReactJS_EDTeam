import React from 'react';
import ReactDOM from 'react-dom';
// import './index.css';
import reportWebVitals from './reportWebVitals';

import App from './components/App';



const root = document.getElementById('root');
// const elemento = React.createElement(elemento, propiedades, hijos);
//const elemento = React.createElement('h1', { className: 'saludo' }, 'Hola Mundo');

ReactDOM.render( < App / > , root);


reportWebVitals();