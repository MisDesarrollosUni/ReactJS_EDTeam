import React from "react";
import withLoader from "../HOC/withLoader";
import CourseCard from "../molecules/CourseCard";

const CourseGrid = ({ courses }) => (
  <div className="ed-grid m-grid-4">
    {
      //    {/* Recorrer el objeto con map */}
      courses.map((c) => (
        <CourseCard
          // key={c.id}
          id={c.id}
          image={c.image}
          title={c.title}
          teacher={c.teacher}
          price={c.price}
        />
      ))
    }
  </div>
);

export default withLoader('courses')(CourseGrid);
