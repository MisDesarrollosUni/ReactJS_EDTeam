import React from "react";
import "../styles/styles.scss";

import Formulario from "./pages/Formulario";
import Courses from "./pages/Courses";
import Course from "./pages/Course";
import MainMenu from "./organisms/MainMenu";
import History from "./pages/History";
import Users from "./pages/Users";

import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Home from "./pages/Home";

const App = () => (
  <Router>
    <MainMenu />
    <Switch>
      {/* A donde (home), sí es exacto, y que se va a imprimir */}{" "}
      <Route path="/" exact component={Home} />
      <Route path="/Cursos/:id" component={Course} />
      <Route path="/Cursos" component={Courses} />
      <Route path="/History" component={History} />
      <Route path="/Usuarios" component={Users} />
      <Route
        path="/Formulario"
        component={() => <Formulario nameForm="página de contacto" />}
      />
      {/* Si no se le define a donde, muestra esta ruta, como pagina no encontrada */}
      <Route
        component={() => (
          <div className="ed-grid">
            <h1> Error 404 </h1> <span> Página no encontrada </span>{" "}
          </div>
        )}
      />
    </Switch>
  </Router>
);

export default App;
