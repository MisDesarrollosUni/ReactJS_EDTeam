import React from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";

// props contiene todas las propiedades que se le manden desde el componenete
const CourseCard = ({ id, image, title, teacher, price }) => (
  <article className="card">
    <div className="img-container s-ratio-16-9 s-radius-tr s-radius-tl">
      <Link to={`/Cursos/${id}`}>
        <img src={image} alt={title} />
      </Link>
    </div>
    <div className="card__data s-border s-radius-br s-radius-bl s-pxy-2">
      <h3 className="t5 s-mb-2 s-center">{title}</h3>
      <div className="s-mb-2 s-main-center">
        <div className="card__teacher s-cross-center">
          <div className="card__avatar s-mr-1">
            <div className="circle img-container">
              <img src="https://ux.ed.team/img/profesor.jpg" alt="Profesor" />
            </div>
          </div>
          <span className="small">{teacher}</span>
        </div>
      </div>
      <div className="s-main-center">
        <a className="button--ghost-alert button--tiny" href="https://ed.team">
          $ {price} USD
        </a>
      </div>
    </div>
  </article>
);

// ESTO DEBE IR DESPUES DEL COMPONENETE PARA QUE LO ENCUENTRE

// Que tipo de datos espero en mi componente
CourseCard.propTypes = {
  image: PropTypes.string,
  title: PropTypes.string,
  teacher: PropTypes.string,
  price: PropTypes.number,
};

// Cuales serán sus valores por defecto
CourseCard.defaultProps = {
  image:
    "https://wpklik.com/wp-content/uploads/2019/03/A-404-Page-Best-Practices-and-Design-Inspiration.jpg",
  title: "Uups... Este curso no se encontró",
  teacher: "No existe el profesor",
  price: -999,
};

export default CourseCard;
