import React, { Component } from "react";

class Formulario extends Component {
  constructor(props) {
    super(props);

    this.state = {
      // Queremos que esto sea dinámico
      name: "",
      email: "",
      fecha: new Date(),
    };

    //Se les debe hacer bind para que se reconozca este metodo dentro de esta clase
    this.cambiarNombre = this.cambiarNombre.bind(this);
    this.cambiarCorreo = this.cambiarCorreo.bind(this);
    this.cambiarFecha = this.cambiarFecha.bind(this);
  }

  cambiarNombre(esteInput) {
    this.setState({
      name: esteInput.target.value,
    });
  }

  cambiarCorreo(esteInput) {
    this.setState({
      email: esteInput.target.value,
    });
  }

  cambiarFecha() {
    this.setState({
      fecha: new Date(),
    });
  }

  render() {
    return (
      <div className="ed-grid">
        <h1>Formulario {`${this.props.nameForm}`}</h1>
        <h4>Fecha: {Math.ceil(this.state.fecha / 1000)}</h4>
        <form className="ed-container" id="elemento">
          <div className="ed-item l-50 form__item">
            <label htmlFor="name">Nombre Completo</label>
            <input type="text" id="name" onChange={this.cambiarNombre} />
          </div>

          <div className="ed-item l-50 form__item">
            <label htmlFor="email">Correo electrónico</label>
            <input type="email" id="email" onChange={this.cambiarCorreo} />
          </div>

          <div className="ed-item form__item">
            <input
              className="button third-color full"
              type="submit"
              value="Enviar"
            />
          </div>
        </form>
        <div className="ed-grid l-section">
          <h2>{`Hola ${this.state.name}`}</h2>
          <span>{`Tu correo es: ${this.state.email}`}</span>
        </div>
      </div>
    );
  }

  componentDidMount() {
    //   Ejecutar acciones despues de haber renderizado el render
    let elemento = document.getElementById("elemento");
    console.log(elemento);
    // this.intervaloFecha = setInterval(() => {
    //   this.cambiarFecha();
    // }, 1000);
  }

  componentDidUpdate(prevProps, prevState) {
    // Obtenemos el estado anterior, además que este metodo nos dice si hubo un cambio
    console.log(prevProps);
    console.log(prevState);
    console.log("--------");
  }

  componentWillUnmount() {
    // Cuando se cambie de vista, se cierre, podemos cerrar varias cosas de nuestra vista o si no
    //   esta seguira ejecutandose n veces
    clearInterval(this.intervaloFecha);
  }
}

export default Formulario;
