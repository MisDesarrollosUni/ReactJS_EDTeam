import React, { useState, useEffect } from "react";
import useCourse from "../CustomHooks/useCourse";

const Course = (props) => {
  //COn esto ya accedemos a las propiedades
 
  const [comment, setComment] = useState("Sin comentarios");

  const course = useCourse(props.match.params.id);
 
  const myComment = (e) =>{
    setComment( e.target.value)
  }

  return course ? (
    <div className="ed-grid">
      {/* Este id que esta en match.params.id es de el archivo App /Cursos/:id   <-- este mismo parametro */}
      <div className="l-block">
        <h1 className="m-cols-3">{course.title}</h1>
        <img className="m-cols-1" src={course.image} alt={course.title} />
        <p className="m-cols-2">Profesor: {course.teacher}</p>
        <p className="m-cols-2">Precio: {course.price}</p>
        {/* Con bind declaro la funcion, con this y le paso elnuevo nombre */}
        {/* <button onClick={changeTitle.bind(this, "Go desde cero")}>Cambiar titulo curso</button> */}
      </div>

      <div className="ed-grid">
        <h2>Escribe tu comentario</h2>
        <input type="text" placeholder="Escribe..." onChange={myComment.bind(this)}/>
        <p>{ comment }</p>
      </div>
    </div>
  ) : (
    <div className="ed-grid">
      <h1>Error 404</h1>
      <span>Curso no encontrado</span>
    </div>
  );
};
export default Course;


 // aqui lo cambio, pero debo usar ...state para mantener los demas datos y colo actualizar uno
  // const changeTitle =(text) =>{
  //   setState({
  //     ...state,
  //     title: text
  //   })
  // }