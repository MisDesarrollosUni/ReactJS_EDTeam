import React, { Component } from "react";
import UsersGrid from '../organisms/UsersGrid';
import axios from "axios";

class Users extends Component {
  constructor(props) {
    super(props);

    this.state = {
      users: [],
    };
  }

  componentDidMount() {
    axios.get("https://jsonplaceholder.typicode.com/users").then((resp) => {
      this.setState({
        users: resp.data,
      });
    });
  }

  render() {
    const { users } = this.state;

    return <UsersGrid users={users}/>;
  }

  //   componentDidMount() {
  //     fetch("https://jsonplaceholder.typicode.com/users", {
  //       method: "get",
  //     })
  //       .then((response) => response.json())
  //       .then((response2) => {
  //           this.setState({
  //               users: response2
  //           })
  //       })
  //   }

  
}

export default Users;
